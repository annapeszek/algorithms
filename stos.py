class Stack:
    def __init__(self):
        self.items = []

    def is_empty(self):
        return not self.items

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def __str__(self):
        return str(self.items)


def check_is_equation_correct(equation: str):
    stack_1 = Stack()
    for i in equation:
        stack_1.push(i)
    number_left = 0
    number_right = 0
    while not stack_1.is_empty():
        char = stack_1.pop()
        if char == ')':
            number_left += 1
        elif char == '(':
            number_right += 1
        if number_left < number_right:
            return False
    if number_left == number_right:
        return True
    else:
        return False


def count_brackets(equation: str):
    bracket_dict = {")": "(", "]": '[', '>': '<'}
    stack = Stack()
    for i in equation:
        if i in bracket_dict.values():
            stack.push(i)
        elif i in bracket_dict.keys():
            if stack.is_empty():
                return False
            bracket = stack.pop()
            if i in bracket_dict:
                if bracket != bracket_dict[i]:
                    return False
    return stack.is_empty()


if __name__ == '__main__':
    # stack = Stack()
    # print(stack.is_empty())
    # print(stack)
    eq = '9*(4)'
    # stack_1 = Stack()
    # for i in eq:
    #     stack_1.push(i)
    print(check_is_equation_correct(eq))
    print(check_is_equation_correct('(999))'))
    print(check_is_equation_correct('((999))'))
    print(check_is_equation_correct(')(()'))
    print(15 * "*")
    print(check_is_equation_correct("(1+2)-(4*5)"))
    print(check_is_equation_correct(")1+2(-(4*5)"))
    print(check_is_equation_correct("((1+2)))"))
    print(check_is_equation_correct("(1)"))
    print(check_is_equation_correct("8*(1)"))
    print(15 * "*")
    print(count_brackets("(1+2)-(4*5)"))
    print(count_brackets(")1+2(-(4*5)"))
    print(count_brackets("((1+2)))"))
    print(count_brackets("(1)]"))
    print(count_brackets("<[(8*1])>"))
    print(count_brackets("<[(8*1)]>"))
