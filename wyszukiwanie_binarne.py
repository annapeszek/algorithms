import time

from random import random, randint


def make_list_with_random_numbers():
    list_1 = []
    for _ in range(0, 100000):
        x = randint(-1000, 1000) + random()
        # x = randint(-1000, 1000)
        list_1.append(x)
    return list_1


def binary_search(list_of_elements, item):
    middle = list_of_elements[len(list_of_elements) // 2]
    low = list_of_elements[0]
    high = list_of_elements[len(list_of_elements) - 1]
    while middle != item:
        if item < middle:
            high = middle
            new_list = list_of_elements[list_of_elements.index(low):list_of_elements.index(middle)]
            middle = new_list[len(new_list) // 2]
        else:
            low = middle
            new_list = list_of_elements[list_of_elements.index(middle):list_of_elements.index(high)]
            middle = new_list[len(new_list) // 2]
    return list_of_elements.index(middle)


def better_binary_search(list_of_elements, item):
    middle = list_of_elements[len(list_of_elements) // 2]
    low = list_of_elements[0]
    high = list_of_elements[len(list_of_elements) - 1]
    while middle != item:
        if item < middle:
            high = middle
            new_list = list_of_elements[list_of_elements.index(low):list_of_elements.index(middle)]
            middle = new_list[len(new_list) // 2]
        else:
            low = middle
            new_list = list_of_elements[list_of_elements.index(middle):list_of_elements.index(high)]
            middle = new_list[len(new_list) // 2]
        if len(new_list) == 2:
            if middle == item:
                list_of_elements.index(middle)
            else:
                if abs(new_list[0] - item) > abs(new_list[1] - item):
                    return list_of_elements.index(new_list[1])
                else:
                    return list_of_elements.index(new_list[0])
    return list_of_elements.index(middle)


def binary_recursion_search(list_of_elements, item):
    if len(list_of_elements) == 1:
        return 0
    if item == list_of_elements[len(list_of_elements) // 2]:
        return list_of_elements.index(item)
    elif item > list_of_elements[len(list_of_elements) // 2]:
        return binary_recursion_search(list_of_elements[len(list_of_elements) // 2:], item) + len(list_of_elements) // 2
    else:
        return binary_recursion_search(list_of_elements[:len(list_of_elements) // 2], item)





def selection_search(list_of_elements, item):
    for element in list_of_elements:
        if element == item:
            return list_of_elements.index(element)


if __name__ == '__main__':
    list_1 = [6, 68, 8, 7, 12, 1, 5, 16, 90]
    list_1.sort()
    print(list_1)
    # print(binary_recursion_search(list_1, 68))
    # print(binary_recursion_search(list_1, 67))
    # print(binary_recursion_search(list_1, 11))
    # print(binary_recursion_search(list_1, 1))
    # print(binary_recursion_search(list_1, 2))

    print(better_binary_search(list_1, 68))
    print(better_binary_search(list_1, 67))
    print(better_binary_search(list_1, 12))
    print(better_binary_search(list_1, 13))
    print(better_binary_search(list_1, 1))
    print(better_binary_search(list_1, 2))

 
    # random_list = make_list_with_random_numbers()
    # value = random_list[randint(0, len(random_list) - 1)]
    # print(value)
    # print(f'index= {random_list.index(value)}')
    #
    # print(20 * '*')
    # print('selection_search')
    # start = time.time()
    # print(selection_search(random_list, value))
    # print(f'time:{time.time() - start}')
    #
    # print(20 * '*')
    # print('binary_search')
    # start = time.time()
    # random_list.sort()

    # start_while = time.time()
    # print(binary_search(random_list, value))
    # print(f'while {time.time() - start_while}')

    # start_recr = time.time()
    # print(binary_recursion_search(random_list, value))
    # print(f'recr: {time.time() - start_recr}')
    #
    # print(f'time:{time.time() - start}')
    # print(f'sprawdzenie indeksu: {random_list.index(value)}')
