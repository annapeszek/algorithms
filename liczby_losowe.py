import pprint
from collections import Counter
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def generate_random_number(x_0, a, c, m):
    return (a * x_0 + c) % m


if __name__ == '__main__':
    seed = 123456789
    a = 101427
    c = 321
    m = 2 ** 16

    x = generate_random_number(seed, a, c, m)
    number_dict = Counter()
    i = 0
    x_1 = []
    x_2 = []
    x_3 = []

    while i < 99999:
        i += 1
        x = generate_random_number(x, a, c, m)
        number_dict[x] += 1
        if i % 3 == 1:
            x_1.append(x)
        elif i % 3 == 2:
            x_2.append(x)
        else:
            x_3.append(x)

    print(number_dict)
    # pprint.pprint(number_dict)

    # plt.scatter(x_2, x_1)
    # plt.scatter(x_1, x_2 , x_3)

    # plt.legend([f'{a,c,m}'], loc='upper left')
    # plt.savefig(f'{a,c,m}')

    # ax = plt.axes(projection='3d')
    # ax.scatter(x_1, x_2, x_3,c=x_3, cmap='r', marker='o')
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # ax.scatter(x_1, x_2, x_3, c='r', marker=0)
    ax.scatter(x_1, x_2, x_3, c=x_3, cmap='viridis', marker=0)
    # ax.plot_trisurf(x_1, x_2, x_3,  cmap='viridis', edgecolor = 'none')
    plt.savefig('3d')
    plt.show()
