from random import random, randint
from guppy import hpy


def make_list_with_100000_numbers():
    return list(range(1, 100001))


def make_list_with_random_numbers():
    list_1 = []
    for _ in range(0, 100000):
        x = randint(-1000, 1000) + random()
        list_1.append(x)
    return list_1


class ContextManager:
    def __init__(self, file_name, method):
        self.file_obj = None
        self.file_name = file_name
        self.method = method

    def __enter__(self):
        self.file_obj = open(self.file_name, self.method)
        return self.file_obj

    def __exit__(self, type, value, traceback):
        self.file_obj.close()


def generate_number(path):
    with open(path, 'r') as file:
        while True:
            number = file.read(6)
            if len(number) < 5:
                break
            yield number[:5]

        # for i in file.read().split('-'):
        #     yield i


def generate():
    yield 'g'
    yield 'e'
    yield 'n'
    yield 'e'
    yield 'r'
    yield 'a'
    yield 't'
    yield 'o'
    yield 'r'


if __name__ == '__main__':
    # print(make_list_with_100000_numbers())
    # make_list_with_random_numbers()
    #
    # path = 'text.txt'
    # with ContextManager(path, 'a') as file:
    #     file.write('\nbbbbcccc')
    #
    path_2 = 'numbers.txt'
    #
    # with ContextManager(path_2, 'a') as file:
    #     content = "-".join([str(randint(10000, 99999)) for _ in range(0, 204800)])
    #     file.write(content)
    #
    path_3 = 'numbers_xxx.txt'
    #
    # with ContextManager(path_3, 'a') as file:
    #     for i in range(1, 3):
    #         content = "-".join([str(randint(10000, 99999)) for _ in range(0, 10)])
    #         file.write(content)
    # generator_1 = generate()
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    # print(generator_1.__next__())
    #
    #
    # print(15 * "*")
    # # generator_2 = generate()  tak nie musimy jeżeli wykorzystujemy for
    # for x in generate():
    #     print(x)
    for x in generate_number(path_2):
        print(x)
    # print(generate_number(path_3))

    h = hpy()
    print(h.heap())
