import heapq

if __name__ == '__main__':
    array = [5, 4, 7, 9, 1, 1, 3, 10]
    heapq.heapify(array)
    print(array)
    array_2 = [46, 4, 78, 9, 3, 5, 79, 87, 15, 4, 3]
    heapq.heapify(array_2)
    print(array_2)
    array_3 = [46, 4, 78, 9, 3, 5, 79, 87, 15, 4, 3]
    heap1 = []
    for element in array_3:
        heapq.heappush(heap1, element)
    print(heap1)

    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))
    print(heapq.heappop(heap1))