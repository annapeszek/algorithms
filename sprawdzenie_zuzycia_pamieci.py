from guppy import hpy


class ContextManager:
    def __init__(self, file_name, method):
        self.file_obj = None
        self.file_name = file_name
        self.method = method

    def __enter__(self):
        self.file_obj = open(self.file_name, self.method)
        return self.file_obj

    def __exit__(self, type, value, traceback):
        self.file_obj.close()


def generate_number(path):
    with open(path, 'r') as file:
        while True:
            number = file.read(6)
            if len(number) < 5:
                break
            yield number[:5]


def generator_2(path):
    with open(path, 'r') as file:
        for i in file.read().split('-'):
            yield i


def generator_3(path):
    with open(path, 'r') as file:
        content = file.read().split('-')
        for i in content:
            yield i


def value_generator(file, separator):
    with open(file, 'r') as file_obj:
        for line in file_obj:
            text = ''
            for sign in line:
                if sign != separator:
                    text += sign
                else:
                    yield text + ' '
                    text = ''


if __name__ == '__main__':

    path_2 = 'numbers.txt'

    path_3 = 'numbers_xxx.txt'

    h = hpy()
    print(h.heap())

    print('value_generator')
    for x in value_generator(path_2, '-'):
        pass

    h = hpy()
    print(h.heap())

    print('generate_number')
    for x in generate_number(path_2):
        pass

    h = hpy()
    print(h.heap())

    print('generator_2')
    for x in generator_2(path_2):
        pass

    h = hpy()
    print(h.heap())

    print('generator_3')
    for x in generator_3(path_2):
        pass

    h = hpy()
    print(h.heap())
